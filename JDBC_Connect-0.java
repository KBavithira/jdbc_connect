package bcas.ap.dp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class JDBC_Connect {
	//private static DriverManager DirverManager;

	public static void main(String[] args) {
		
		System.out.println("MYQL JDBC CONNECTION TEST");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.err.println("JDBC driver is not attached");
			e.printStackTrace();
		}
		
		Connection connection = null;
		try {
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/csd16","root","root");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(connection!= null) {
			System.out.println(" Success !!! Database was connected. ");
		}else {
			System.out.println(" Failed to make connection !!! ");
		}
		
	}

}
